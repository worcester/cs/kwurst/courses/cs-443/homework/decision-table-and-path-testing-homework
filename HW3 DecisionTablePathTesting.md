# Homework 3 &mdash; Decision Table and Path Testing

## Instructions

### Decision Tables

For the **Decision Table** questions below, use the following function:

`int discountPercent(double price, int pastOrderCount)`

returns the percent discount to be given, when given the price of the merchandise, and the number of orders the customer has placed in the past.

Past Order Count | Percent Discount
--- | ---
pastOrderCount < 10 | 0%
10 ≤ pastOrderCount ≤ 40 | 5%
40 < pastOrderCount | 10%

Orders over $1000.00 are given an automatic, additional 1% discount.

### Path Testing

For the **Path Testing** questions below, use the following code:

```java
 1. public void play() {
 2.     String src = null;
 3.     String dest = null;
 4.     LinkedList<Card> srcPile = null;
 5.     boolean gameOver = false;
 6.  
 7.     while (!gameOver) {
 8.         displayTableau();
 9.
10.         do {
11.             System.out.print("\n Move top card from (D, W0, W1, W2, W3, or Q to quit): \n");
12 .            src = in.nextLine();
13.             if (src.toUpperCase().charAt(0) == 'Q') {
14.                 gameOver = true;
15.             }
16.         } while (!gameOver && (srcPile = getSrcPile(src)) == null);
17. 
18.         if (!gameOver) {
19.             System.out.print("\n to (F0, F1, F2, F3, W0, W1, W2, W3): \n");
20.             dest = in.nextLine();
21.             addToDestPile(dest, srcPile);
22.         }
23. 
24.          gameOver = checkForGameOver(gameOver);
26.     }
27. }    
```

## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

### Decision Table

1. Create a Decision Table for the `discountPercent` function. Follow a process like the example in Activity 7.

    1. Specify the conditions and actions.
    2. If it makes sense to use equivalence classes, specify the equivalence classes to simplify your table.

### Program Graphs

1. Create the Program Graph (as in Activity 8) for the `play()` method given above.

### DD-Path Graphs

1. Create the DD-Path Graph (as in Activity 8) for the `play()` method given above.

    * Make sure you include the table translating from Program Graph nodes (from your graph above), with the corresponding DD-Path node labeled with a letter.

## *Intermediate Add-On*

**Complete this portion in an Acceptable fashion to apply towards base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Rule Counts

1. What is the maximum number of rules for this function (if we do not collapse any.)
2. Show your Decision Table from the Base Assignment
    * Annotate the columns with the number of rules each covers.

### DD-Path Node Case Definitions

1. For the DD-Path Graph you created in the Base Assignment, add the Case Definition (1-5) to your table for each node in that DD-Path Graph.

## *Advanced Add-On*

**Complete this portion in an Acceptable fashion to apply towards base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many Advanced Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Robust Testing

1. How would you implement Robust testing with your decision table?
    * What conditions would you add to your table?
    * What actions would you add to your table?
    * What would be the maximum number of rules now?

### DU-Paths

Beginning with your DD-Path graph for `play()` create:

1. The table of variables with columns for *variable name*, *defined at node*, and *used at node*, as in Activity 9.
2. The table of ***all*** DU Paths with columns for *variable name*, *path (beginning, end) nodes*, and *definition clear?*, as in Activity 9.

## Specification

You may write your answers using any program that you wish.
You should choose a program that allows you to create tables
and draw graphs. ***You must save your answers as a
PDF file and upload that PDF to the HW3 Assignment in
Blackboard.***

To be considered Acceptable your assignment must:

* Be submitted in PDF format.

## Due Date

18 March 2024 - 23:59

&copy; 2024 Karl R. Wurst <karl@w-sts.com>

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
